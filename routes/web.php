<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MatchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/livescore');
});
Route::get('/livescore', [MatchController::class,'show'])->name('show');
Route::get('/livescore/{type}', [MatchController::class,'show'])->name('show');