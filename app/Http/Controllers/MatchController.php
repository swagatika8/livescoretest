<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MatchController extends Controller
{
    public function __construct()
    {
            
    }

    public function show(Request $request)
    {
    	if(isset($request->type) && $request->type == 'cricket'){
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=cricket";
    	}
    	elseif(isset($request->type) && $request->type == 'soccer'){
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=soccer";
    	}elseif(isset($request->type) && $request->type == 'basketball'){
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=basketball";
    	}elseif(isset($request->type) && $request->type == 'tennis'){
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=tennis";
    	}elseif(isset($request->type) && $request->type == 'hockey'){
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=hockey";
    	}else{
    		$url ="https://livescore6.p.rapidapi.com/matches/v2/list-live?Category=cricket";
    	}
    	
$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: livescore6.p.rapidapi.com",
		"x-rapidapi-key: 435f87eee5mshb7e900e49248c04p18888fjsndf74baa572f3"
	],
]);

$response1 = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
    	$response =json_decode($response1);

    	foreach ($response->Stages as $Stages) {
    		$Events = $Stages->Events;
    	}
    	if(isset($Events)){
    	foreach ($Events as $events) {
    			$Pids = get_object_vars($events->Pids);
    			$Sids=get_object_vars($events->Sids);
    			$T1=$events->T1;
    			$T2=$events->T2;
    			$stg=get_object_vars($events->Stg);
    		}
    		foreach ($T1 as $t1) {
    			$t1pids = get_object_vars($t1->Pids);
    		}
    		foreach ($T2 as $t2) {
    			$t2pids = get_object_vars($t2->Pids);
    		}
    		return view('match.index',compact('response','Pids','Sids','T1','T2','stg','t1pids','t2pids'));
    	}else{
          return Redirect::to('/livescore');
    	}

      
       
    }
}
