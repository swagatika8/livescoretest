<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<div class="container">
  <div class="row">
    <div class="col-md-12   text-center">
                <h4>
                  <span>Live Score</span>
                </h4>
                <a class="btn btn-primary" href="{{ url('/livescore', 'soccer')}}">soccer</a>
                <a class="btn btn-primary" href="{{ url('/livescore', 'cricket')}}">cricket</a>
                <a class="btn btn-primary" href="{{ url('/livescore', 'basketball')}}">basketball</a>
                <a class="btn btn-primary" href="{{ url('/livescore', 'tennis')}}">tennis</a>
                <a class="btn btn-primary" href="{{ url('/livescore', 'hockey')}}">hockey</a>
              </div>
    <table id="example" class="table table-striped  table-responsive" >
        <thead>
            <tr>
                <th>Sid</th>
                <th>Snm</th>
                <th>Sds</th>
                <th>Scd</th>
                <th>Cid</th>
                <th>Cnm</th>
                <th>Csnm</th>
                <th>Ccd</th>
                <th>Ccdiso</th>
                <th>Scu</th>
                <th>Chi</th>
                <th>Shi</th>
                <th>Sdn</th>
            </tr>
        </thead>
        <tbody>
          @foreach($response->Stages as $stages)
            <tr>
                <td>{{$stages->Sid}}</td>
                <td>{{$stages->Snm}}</td>
                <td>{{$stages->Sds}}</td>
                <td>{{$stages->Scd}}</td>
                <td>{{$stages->Cid}}</td>
                <td>{{$stages->Cnm}}</td>
                <td>{{$stages->Csnm}}</td>
                <td>{{$stages->Ccd}}</td>
                <td>{{$stages->Ccdiso}}</td>
                <td>{{$stages->Scu}}</td>
                <td>{{$stages->Chi}}</td>
                <td>{{$stages->Shi}}</td>
                <td>{{$stages->Sdn}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <h4>
                  <span>Events</span>
                </h4>
    <table id="example" class="table table-striped table-bordered table-responsive" style="width:100%">
        <thead>
            <tr>
                <th>Eid</th>
                <th>Tr1C1</th>
                <th>Tr2C1</th>
                <th>Tr1CW1</th>
                <th>Tr2CW1</th>
                <th>Tr1CO1</th>
                <th>Tr2CO1</th>
                <th>Eps</th>
                <th>Esid</th>
                <th>EpsL</th>
                <th>Epr</th>
                <th>Ecov</th>
                <th>ErnInf</th>
                <th>Et</th>
                <th>EtTx</th>
                <th>ECo</th>
                <th>Ebat</th>
                <th>TPa</th>
                <th>TCho</th>
                <th>Esd</th>
                <th>Ese</th>
                <th>LuUT</th>
                <th>Exd</th>
                <th>Eact</th>
                <th>IncsX</th>
                <th>LuX</th>
                <th>StatX</th>
                <th>SubsX</th>
                <th>SDFowX</th>
                <th>SDInnX</th>
                <th>EO</th>
                <th>LuC</th>
                <th>Ehid</th>
                <th>Pid</th>
                <th>Spid</th>
            </tr>
        </thead>
        <tbody>
          
          @foreach($response->Stages as $stages)
          @foreach($stages->Events  as $events)
            <tr>
                <td>{{$events->Eid}}</td>
              @if(isset($events->Tr1C1))
                <td>{{$events->Tr1C1}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($events->Tr2C1))
                <td>{{$events->Tr2C1}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($events->Tr1CW1))
                <td>{{$events->Tr1CW1}}</td>
                 @else 
                <td></td>
                @endif
                @if(isset($events->Tr2CW1))
                <td>{{$events->Tr2CW1}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($events->Tr1CO1))
                <td>{{$events->Tr1CO1}}</td>
                @else 
                <td></td>
                @endif
                @if(isset($events->Tr2CO1))
                <td>{{$events->Tr2CO1}}</td>
                @else 
                <td></td>
                 @endif
                  @if(isset($events->Eps))
                <td>{{$events->Eps}}</td>
                @else 
                <td></td>
                 @endif
                 @if(isset($events->Esid))
                <td>{{$events->Esid}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->EpsL))
                <td>{{$events->EpsL}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->Epr))
                <td>{{$events->Epr}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->Ecov))
                <td>{{$events->Ecov}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->ErnInf))
                <td>{{$events->ErnInf}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->Et))
                <td>{{$events->Et}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->EtTx))
                <td>{{$events->EtTx}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->ECo))
                <td>{{$events->ECo}}</td>
                @else 
                <td></td>
                 @endif
                @if(isset($events->Ebat))
                <td>{{$events->Ebat}}</td>
                @else 
                <td></td>
                 @endif
                @if(isset($events->TPa))
                <td>{{$events->TPa}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->TCho))
                <td>{{$events->TCho}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Esd))
                <td>{{$events->Esd}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Ese))
                <td>{{$events->Ese}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->LuUT))
                <td>{{$events->LuUT}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Exd))
                <td>{{$events->Exd}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Eact))
                <td>{{$events->Eact}}</td>
                   @else 
                <td></td>
                 @endif
                @if(isset($events->IncsX))
                <td>{{$events->IncsX}}</td>
                   @else 
                <td></td>
                 @endif
                @if(isset($events->ComX))
                <td>{{$events->ComX}}</td>
                   @else 
                <td></td>
                 @endif
                @if(isset($events->LuX))
                <td>{{$events->LuX}}</td>
                 @else 
                <td></td>
                 @endif
                @if(isset($events->StatX))
                <td>{{$events->StatX}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->SubsX))
                <td>{{$events->SubsX}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->SDFowX))
                <td>{{$events->SDFowX}}</td>
                   @else 
                <td></td>
                 @endif
                @if(isset($events->SDInnX))
                <td>{{$events->SDInnX}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->EO))
                <td>{{$events->EO}}</td>
                @else 
                <td></td>
                 @endif
                @if(isset($events->LuC))
                <td>{{$events->LuC}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Ehid))
                <td>{{$events->Ehid}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Pid))
               
                <td>{{$events->Pid}}</td>
                  @else 
                <td></td>
                 @endif
                @if(isset($events->Spid))
                <td>{{$events->Spid}}</td>
                  @else 
                <td></td>
                 @endif

            </tr>
            @endforeach
             @endforeach
        </tbody>
    </table>
     <h4>
                  <span>Pids</span>
                </h4>
    <table id="example" class="table table-striped  table-responsive">
        <thead>
            <tr>
                <th>8</th>
                <th>11</th>
                <th>12</th>
            </tr>
        </thead>
        <tbody>
          
            <tr>
              @if(isset($Pids['8']))
                <td>{{$Pids['8']}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($Pids['11']))
                <td>{{$Pids['11']}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($Pids['12']))
                <td>{{$Pids['12']}}</td>
                 @else 
                <td></td>
                @endif
            </tr>
        </tbody>
    </table>
     <h4>
                  <span>Sids</span>
                </h4>
     <table id="example" class="table table-striped  table-responsive" >
        <thead>
            <tr>
                <th>8</th>
                <th>11</th>
                <th>12</th>
            </tr>
        </thead>
        <tbody>
          
            <tr>
              @if(isset($Sids['8']))
                <td>{{$Sids['8']}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($Sids['11']))
                <td>{{$Sids['11']}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($Sids['12']))
                <td>{{$Sids['12']}}</td>
                 @else 
                <td></td>
                @endif
            </tr>
        </tbody>
    </table>
    <h4>
                  <span>T1</span>
                </h4>
  <table id="example" class="table table-striped  table-responsive" >
        <thead>
            <tr>
                <th>Nm</th>
                <th>ID</th>
                <th>tbd</th>
                <th>Img</th>
                <th>Gd</th>
                <th>Pids-1</th>
                <th>Pids-6</th>
                <th>Pids-8</th>
                <th>Pids-12</th>
                <th>CoNm</th>
                <th>CoId</th>
                <th>HasVideo</th>
            </tr>
        </thead>
        <tbody>
          @foreach($T1 as $t1)
            <tr>
              @if(isset($t1->Nm))
                <td>{{$t1->Nm}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($t1->ID))
                <td>{{$t1->ID}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t1->tbd))
                <td>{{$t1->tbd}}</td>
                 @else 
                <td></td>
                @endif
                @if(isset($t1->Img))
                <td>{{$t1->Img}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1->Gd))
                <td>{{$t1->Gd}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1pids['Pids-1']))
                <td>{{$t1pids['Pids-1']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1pids['Pids-6']))
                <td>{{$t1pids['Pids-6']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1pids['Pids-8']))
                <td>{{$t1pids['Pids-8']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1pids['Pids-12']))
                <td>{{$t1pids['Pids-12']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t1->CoNm))
                <td>{{$t1->CoNm}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t1->CoId))
                <td>{{$t1->CoId}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t1->HasVideo))
                <td>{{$t1->HasVideo}}</td>
                @else 
                <td></td>
                @endif

            </tr>
            @endforeach
        </tbody>
    </table>
    <h4>
                  <span>T2</span>
                </h4>
  <table id="example" class="table table-striped  table-responsive" >
        <thead>
            <tr>
                <th>Nm</th>
                <th>ID</th>
                <th>tbd</th>
                <th>Img</th>
                <th>Gd</th>
                <th>Pids-1</th>
                <th>Pids-6</th>
                <th>Pids-8</th>
                <th>Pids-12</th>
                <th>CoNm</th>
                <th>CoId</th>
                <th>HasVideo</th>
            </tr>
        </thead>
        <tbody>
          @foreach($T2 as $t2)
            <tr>
              @if(isset($t2->Nm))
                <td>{{$t2->Nm}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($t2->ID))
                <td>{{$t2->ID}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t2->tbd))
                <td>{{$t2->tbd}}</td>
                 @else 
                <td></td>
                @endif
                @if(isset($t2->Img))
                <td>{{$t2->Img}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2->Gd))
                <td>{{$t2->Gd}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2pids['Pids-1']))
                <td>{{$t2pids['Pids-1']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2pids['Pids-6']))
                <td>{{$t2pids['Pids-6']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2pids['Pids-8']))
                <td>{{$t2pids['Pids-8']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2pids['Pids-12']))
                <td>{{$t2pids['Pids-12']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($t2->CoNm))
                <td>{{$t2->CoNm}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t2->CoId))
                <td>{{$t2->CoId}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($t2->HasVideo))
                <td>{{$t2->HasVideo}}</td>
                @else 
                <td></td>
                @endif

            </tr>
            @endforeach
        </tbody>
    </table>
    <h4>
                  <span>Stg</span>
                </h4>
  <table id="example" class="table table-striped  table-responsive" >
        <thead>
            <tr>
                <th>Sid</th>
                <th>Snm</th>
                <th>Sds</th>
                <th>Scd</th>
                <th>Cid</th>
                <th>Cnm</th>
                <th>Csnm</th>
                <th>Ccd</th>
                <th>Ccdiso</th>
                <th>Scu</th>
                <th>Chi</th>
                <th>Shi</th>
                <th>Sdn</th>
            </tr>
        </thead>
        <tbody>
          
            <tr>
              @if(isset($stg['Sid']))
                <td>{{$stg['Sid']}}</td>
                 @else 
                <td></td>
                @endif
                 @if(isset($stg['Snm']))
                <td>{{$stg['Snm']}}</td>
                @else 
                <td></td>
                @endif
                  @if(isset($stg['Sds']))
                <td>{{$stg['Sds']}}</td>
                 @else 
                <td></td>
                @endif
                @if(isset($stg['Scd']))
                <td>{{$stg['Scd']}}</td>
                @else 
                <td></td>
                @endif
                 @if(isset($stg['Cid']))
                <td>{{$stg['Cid']}}</td>
                @else 
                <td></td>
                @endif
                @if(isset($stg['Cnm']))
                <td>{{$stg['Cnm']}}</td>
                @else 
                <td></td>
                 @endif
                  @if(isset($stg['Csnm']))
                <td>{{$stg['Csnm']}}</td>
                @else 
                <td></td>
                 @endif
                  @if(isset($stg['Ccd']))
                <td>{{$stg['Ccd']}}</td>
                @else 
                <td></td>
                 @endif
                 @if(isset($stg['Ccdiso']))
                <td>{{$stg['Ccdiso']}}</td>
                @else 
                <td></td>
                 @endif
                  @if(isset($stg['Scu']))
                <td>{{$stg['Scu']}}</td>
                @else 
                <td></td>
                 @endif
                   @if(isset($stg['Chi']))
                <td>{{$stg['Chi']}}</td>
                @else 
                <td></td>
                 @endif
                    @if(isset($stg['Shi']))
                <td>{{$stg['Shi']}}</td>
                @else 
                <td></td>
                 @endif
                   @if(isset($stg['Sdn']))
                <td>{{$stg['Sdn']}}</td>
                @else 
                <td></td>
                 @endif

            </tr>
        </tbody>
    </table>
    
  </div>
</div>
<style type="text/css">
  table{
    width:100%;
}
#example_filter{
    float:right;
}
#example_paginate{
    float:right;
}
label {
    display: inline-flex;
    margin-bottom: .5rem;
    margin-top: .5rem;
   
}

</style>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable(
        
         {     

      "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
        "iDisplayLength": 5
       } 
        );
} );


function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
</script>